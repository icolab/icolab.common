﻿using System;

namespace IcoLab.Common
{
    public class UtcDateTime : IDateTime
    {
        public DateTime UtcNow => DateTime.UtcNow;
    }
}