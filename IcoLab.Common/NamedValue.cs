﻿namespace IcoLab.Common
{
    public class NamedValue<T>
    {
        public string Name;
        public T Value;

        public NamedValue(string name, T value)
        {
            Name = name;
            Value = value;
        }
    }

    public static class NamedValue
    {
        public static NamedValue<T> Create<T>(string name, T value) => new NamedValue<T>(name, value);
    }
}