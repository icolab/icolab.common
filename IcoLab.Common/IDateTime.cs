﻿using System;

namespace IcoLab.Common
{
    public interface IDateTime
    {
        DateTime UtcNow { get; }
    }
}