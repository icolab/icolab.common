﻿using System;

namespace IcoLab.Common
{
    public class DateUtils
    {
        public static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static DateTime FromUnixTime(double unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static int ToUnixTime (DateTime dt)
        {
            var unixEpoch = new DateTime(1970, 1, 1);
            return (int) dt.Subtract(unixEpoch).TotalSeconds;
        }
    }
}