﻿using System;

namespace IcoLab.Common.Utils
{
    public static class StringExtensions
    {
        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool ContainsIngnoreCase(this string text, string value) 
        {
            return text.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

    }
}