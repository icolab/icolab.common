﻿using System.Collections.Generic;

namespace IcoLab.Common.Utils
{
    public static class DictionaryExtensions
    {
        public static TValue
            GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            dictionary.TryGetValue(key, out TValue value);
            return value;
        }
    }
}