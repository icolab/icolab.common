﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text;

namespace IcoLab.Common.Utils
{
    public static class StringUtils {

        public static string ToStringHex(byte[] value) {
            var output = string.Empty;
            for (var i = 0; i < value.Length; i++) {
                output += value[i].ToString("x2", CultureInfo.InvariantCulture);
            }

            return output;
        }

        public static string ToHttpPostString(List<KeyValuePair<string,string>> nameValueCollection) {

            string Encode(string data)
            {
                if (string.IsNullOrEmpty(data))
                    return string.Empty;
                return Uri.EscapeDataString(data).Replace("%20", "+");
            }

            var stringBuilder = new StringBuilder();

            foreach (var nameValue in nameValueCollection) {
                if (stringBuilder.Length > 0)
                    stringBuilder.Append('&');
                stringBuilder.Append(Encode(nameValue.Key));
                stringBuilder.Append('=');
                stringBuilder.Append(Encode(nameValue.Value));
            }

            return stringBuilder.ToString();

        }
    }
}