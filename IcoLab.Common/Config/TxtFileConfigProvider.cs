﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace IcoLab.Common.Config
{
    public class TxtFileConfigProvider : FileConfigurationProvider
    {
        public TxtFileConfigProvider(FileConfigurationSource source)
            : base(source)
        { }


        public override void Load(System.IO.Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var abiJson = reader.ReadToEnd();
                var fileName = Path.GetFileNameWithoutExtension(Source.Path);
                Data.Clear();
                Data.Add(fileName, abiJson);
            }
        }
    }
}