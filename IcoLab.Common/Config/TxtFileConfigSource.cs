﻿using Microsoft.Extensions.Configuration;

namespace IcoLab.Common.Config
{

	public class TxtFileConfigSource: FileConfigurationSource {
		public override IConfigurationProvider Build(IConfigurationBuilder builder) {
			this.FileProvider = builder.GetFileProvider();
			return new TxtFileConfigProvider(this);
		}
	}
}